<!DOCTYPE html>
<html lang="en">
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Fight Finals Fever!</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="less/fightfinalsfever.less" rel="stylesheet/less" type="text/css">

    <!-- jQuery -->
    <!--<script src="js/jquery.js"></script>-->
    <script>window.jQuery || document.write('<script src="js/jquery.js"><\/script>')</script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>

    <!-- Less compiler -->
    <script type="text/javascript" src="js/less.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <?php
    $i = rand();
    echo "<script type=\"text/javascript\" src=\"js/fightfinalsfever.js?" . $i . "\"></script>";
    ?>






    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">
                    <!--<i class="fa fa-play-circle"></i> --> <span class="light">Fight</span> Finals Fever
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#relax">Relax</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#recipes">Recipes</a>
                    </li>
                    <li>
                        <a class ="page-scroll" href="#exercise">Exercise</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#study">Study</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- End Navigation Block -->

    <!-- Intro Header -->
    <header id="intro" class="intro" data-0="background-position: 0px 0px;" data-600="background-position:0px -200px;" data-anchor-target="#intro">
        <div id="intro-body" class="intro-body">
            <div class="container">
              <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <img src="img/logo.png" class="img-responsive" alt="Logo" data-0="opacity: 1;" data-600="opacity: 0;" data-anchor-target="#intro-body">
                        <div class="col-md-8 col-md-offset-2">
                          <a href="#about" class="btn btn-circle page-scroll" data-0="opacity: 1;" data-500="opacity: 0;" data-anchor-target="#intro-body">
                              <i class="fa fa-angle-double-down animated"></i>
                            </a>
                        </div>
                    </div>
              </div>
            </div>
        </div>
    </header>

    <!-- About Section -->
    <section id="about" class="about-section content-section text-center" data-bottom="background-position: 0px 0px;" data--200-top="background-position:0px -150px;" data-anchor-target="#about">
      <div class="container">
        <div class="row">
            <div id="about-body" class="col-lg-8 col-lg-offset-2" data-bottom="opacity: 0;" data-center="opacity: 1;" data-200-top="opacity: 1;" data--500-top="opacity: 0;">
                <h2>About Fight Final Fever</h2>
                <p>Our mission here at fightfinalsfever.com is to promote brain stimulation, reduce stress, and provide healthy options for diet and exercise.
                   Boasting exercises and activities for healthy active brain function, recipes for optimum nutrition, a calm, soothing, and eclectics playlist
                   for relaxing and studying, and tips and tricks for reducing stress, all to ease the heavy weight and pain of dead week and finals.
                   Follow Fight Finals Fever to de-stress, relax, and indulge in the benefits of a healthy diet and exercise regimen, increase brain activity
                   and produce positive outcomes during finals and beyond.
                  </p>
            </div>
        </div>
      </div>
    </section>

    <!-- Relaxing Section -->
    <section id="relax" class="relax-section content-section text-center" data-bottom="background-position: 0px 0px;" data--200-top="background-position:0px -150px;" data-anchor-target="#relax">
      <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Relaxing during finals week</h2>
                <iframe src="https://embed.spotify.com/?uri=spotify:user:michellebrowning716:playlist:3ORRXS3J0ZOQdcweKFB23n" width="550" height="600" frameborder="0" allowtransparency="true"></iframe>
            </div>
        </div>
      </div>
    </section>

    <!-- Recipes Section -->
    <section id="recipes" class="container content-min text-center">
        <div class="recipe-section">
            <!--<div class="col-lg-8 col-lg-offset-2">-->
                <h2>Recipes to help during finals week</h2>
                <div class="col-lg-12 bottom-buffer">
                  <div class="recipe-selectors">
      							<div id="as-selector" class="a-selector active"><span>Apple<br> Sandwiches</span></div>
      							<div id="gc-selector" class="a-selector"><span>Granola<br> Clusters</span></div>
      							<div id="ppb-selector" class="a-selector"><span>Peanut<br> Power<br> Ball</span></div>
      						</div>
      						<div class="recipes-list">
      							<div class="formatted active" id="formatted-1">
                      <div class="col-md-6">
                        <h4><b>Ingredients</b></h4>
                          <p>
                          <ul>
                            <li>2 apples</li>
                            <li>2-3 tablespoons peanut butter</li>
                            <li>3 tablespoons granola</li>
                            <li>Handful of raisins</li>
                          </ul>
                        </p>
                      </div>
                      <div class="col-md-6">
                        <h4><b>Instructions</b></h4>
                          <p>
                          <ol>
                            <li>Remove cores from apples and cut into 1/4-1/2" thick slices</li>
                            <li>Spread apple slice with peanut butter.</li>
                            <li>Top with granola and raisins.</li>
                            <li>Place another apple slice on top and gently press together.</li>
                            <li>Repeat with remaining apple slices.</li>
                          </ol>
                        </p>
                      </div>
                      <div class="col-md-6 buffer">
                        <img src="img/apples-1.jpg" class="buffer img-responsive img-circle" alt="Apples 1">
                      </div>
                      <div class="col-md-6 buffer">
                        <img src="img/apples-2.jpg" class="img-responsive img-circle" alt="Apples 2">
                      </div>
      							</div>
      							<div class="formatted" id="formatted-2">
                      <div class="col-md-6">
                        <h4><b>Ingredients</b></h4>
                          <p>
                          <ul>
                            <li>1/4 cup of honey</li>
                            <li>1/4 cup of peanut butter</li>
                            <li>3/4 cup of choice of nuts</li>
                            <li>1 1/2 cup of oats</li>
                            <li>3/4 cup of dark chocolate</li>
                          </ul>
                        </p>
                      </div>
                      <div class="col-md-6">
                        <h4><b>Instructions</b></h4>
                          <p>
                          <ol>
                            <li>Preheat oven to 350 degrees.</li>
                            <li>Combine in medium sized bowl the nuts and oats.</li>
                            <li>Heat honey and peanut butter over stove or microwave until having a thin consistency.</li>
                            <li>Pour honey and peanut butter with oats and nuts. Mix together.</li>
                            <li>Form the mixture with hands into small 2in balls, place on cookie sheet, and bake for 10 minutes.</li>
                            <li>Let them cool off.</li>
                            <li>Melt chocolate and dip desired amount of the cluster in melted chocolate.</li>
                            <li>Place on plastic surface to let cool and harden</li>
                            <li>Eat and enjoy (:</li>
                          </ol>
                        </p>
                      </div>
                      <div class="col-lg-8 col-lg-offset-2 buffer">
                        <img src="img/granola.jpg" class="img-responsive img-circle" alt="Granola">
                      </div>
      							</div>
      							<div class="formatted" id="formatted-3">
                      <div class="col-lg-6 col-lg-offset">
                        <h4><b>Ingredients</b></h4>
                          <p>
                          <ul>
                            <li>Banana/Banana Chips</li>
                            <li>Dried Apricot</li>
                            <li>Crushed Teddy Grahams</li>
                            <li>Nuts (Almonds, Cashews, Pistachios, Pecans)</li>
                            <li>Seeds (Pumpkin, Sunflower, Chia, Hemp, Flax)</li>
                            <li>Grated Carrot</li>
                            <li>Unsweetened Coconut Chips</li>
                            <li>Semisweet Chocolate Chips (for that healthier craving of dessert)</li>
                          </ul>
                        </p>
                      </div>
                      <div class="col-md-6">
                        <h4><b>Instructions</b></h4>
                          <p>
                          <ol>
                            <li>Start with a base of no-sugar-added peanut butter in a medium size bowl.</li>
                            <li>Add a small amount of honey (optional) to create a sticky, somewhat thick texture.</li>
                            <li>Next mix in low sugar cereal, oats, or granola to the bowl.</li>
                            <li>Add any number of ingredients from the list below to your peanut butter mixture and squeeze together into a ball.</li>
                            <li>Cool in refrigerator if desired for a colder, firmer tasty treat.</li>
                          </ol>
                        </p>
                      </div>
                      <div class="col-lg-8 col-lg-offset-2 buffer">
                        <img src="img/power-ball.jpg" class="img-responsive img-circle" alt="Power ball">
                      </div>
      						  </div>
                  </div>
                </div>
        </div>
    </section>

    <!-- Exercise Section -->
    <section id="exercise" class="exercise-section content-section text-center">
        <div class="container">
          <div class="row">
              <div class="col-lg-12">

                <div class="col-lg-8 col-lg-offset-2 top-buffer-2" data-500-bottom="opacity: 0;" data-bottom="opacity: 1;" data-200-top="opacity: 1;" data--400-top="opacity: 0;" data-data-anchor-target="#exercise">
                  <h2><u>Exercise to keep up with health during finals week</u></h2>
                  <h3>Is It important To Exercise during Finals week?</h3>
                  <p>
                    The Answer is, 100% yes!! Exercising increases your brain power
                    <ul>
                    <li>When you exercise extra blood is pumped into your brain, giving out oxygen and nutrients that will help your brain function more efficiently.</li>
                    <li>Exercising increases the production of  neurotransmitter or BDNF in your brain that will enhance functions like memory.</li>
                    <li>Exercising also increases the size of your Hippocampus part of the brain which is really important in learning, problem solving and memory.</li>
                    <li>Most importantly when you exercise you are stress free because of the production of endorphins by the hypothalamus and the pituitary gland in the brain.</li>
                  </ul>
                  </p>
                </div>
                <div class="col-lg-8 col-lg-offset-2 top-buffer" data-500-bottom="opacity: 0;" data-200-bottom="opacity: 1;" data-200-top="opacity: 1;" data--400-top="opacity: 0;" data-data-anchor-target="#exercise">
                  <h3>Take a 30 minutes Exercise Break During Finals Week</h3>
                  <p>
                    Your body just needs to exercise it doesn’t matter what kind of exercise you are doing any kind as long as you are moving your muscles and working your body, it stimulates the brain.
                    Some recommended exercise during a stressful week are:
                    <ol>
                      <li>Running</li>
                      <li>Biking</li>
                      <li>The treadmill at the gym</li>
                      <li>Walking is also important incase you are not really into working out</li>
                      <li>For a more relaxing and easy exercise, you can try some yoga</li>
                    </ol>
                  </p>
                </div>
            </div>
          </div>
        </div>
    </section>

    <!-- Study Section -->
    <section id="study" class="study-section text-center" data-bottom="background-position: 0px 0px;" data--200-top="background-position:0px -150px;" data-data-anchor-target="#study">
      <div class="curtainContainer">
        <div class="curtain"
          data-bottom-top="opacity: 0"
          data-106-top="height: 1%; top: -10%; opacity: 0;"
          data-center="height: 100%; top: 0%; opacity: 0.5;"
          data-anchor-target="#study">
        </div>
        <div class="study-container">
            <div id="study-body" class="study-body" data-400-bottom="opacity: 0"
                data-center="opacity: 1;"
                 data-anchor-target="#study  #study-body">
                  <h2>Study techniques for finals</h2>
                  <p>Stress is what our bodies and minds experience as we adapt to a continually changing environment. The response can be triggered by a positive experience such as acing an exam, or by negative experience such as procrastinated on homework, or studying for finals. Stress that continues without relief can lead to a condition called distress, a negative stress reaction. Distress can have a big impact on your lifestyles such as losing your cool, makes you susceptible to diseases, makes you look older, causing premature wrinkles, etc. So, concerning with the situation, we came up with a list of things-to-do to help immunize fellow SPU students for the finals fever season.</p>
                  <div class="study-list">
                      <ul>
                          <li>Deactivate your Facebook or other social networking account (temporarily)</li>
                          <li>Go on fun apps for a quick break. i.e. Trivia crack, Yik Yak, Reddit, etc. </li>
                          <li>Grab some (brain) food</li>
                          <li>Go for a 6 minute run @ the canal, cemetery, around campus. </li>
                          <li>Make origami </li>
                          <li>Put on some <a class="page-scroll" href="#relax">music</a>.</li>
                          <li>Chew a piece of gum</li>
                          <li>Have some tea</li>
                          <li>Do nothing for a break</li>
                          <li>Step outside, breathe in fresh air and enjoy the nature. i.e. stargazing, bird watching, etc.</li>
                          <li>Set a timer to work in intervals</li>
                          <li>Meet with friends and study groups to help study</li>
                          <li>Get a good amount of sleep; don't wait until the last minute!</li>
                          <li>Avoid cramming so that the work is not overwelming</li>
                      </ul>
                  </div>
              </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer>
        <div class="container text-center">
            <p>Copyright &copy; FightFinalFever 2015</p>
        </div>
    </footer>


    <script src="js/skrollr.min.js"></script>
    <script src="js/skrollr.menu.min.js"></script>

</body>

</html>
