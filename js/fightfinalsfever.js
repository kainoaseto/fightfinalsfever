/*!
 * Start Bootstrap - Grayscale Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// jQuery to collapse the navbar on scroll
$(window).scroll(function() {
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
});

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

$(function(){
  var triggers = $('.a-selector'),
      sections = $('.recipes-list'),
      panels = sections.find('.formatted');

      triggers.on('click', function() {
        triggers.removeClass('active');
        $(this).addClass('active');
        var elem = $(this).index();
        var mine = panels.get(elem);
        panels.hide();
        $(mine).fadeIn();
      });

});

/* If this is not done then there will be random issues with browsers depending on
  how they decide to load in the content....the plugin needs to see all the content loaded
  in.
*/
setTimeout(function () {
  skrollr.get().refresh();
}, 1000);

$(document).ready(function() {
  var sections = $('.recipes-list'),
      panels = sections.find('.formatted');

      $('#formatted-1').addClass('active');
      var elem = $('#formatted-1').index();
      var mine = panels.get(elem);
      panels.hide();
      $(mine).fadeIn();


      var s = skrollr.init({
        render: function(data){
          //Debugging - Log position
          //console.log(data.curTop)

        },
        forceHeight: false
      });
});

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});
